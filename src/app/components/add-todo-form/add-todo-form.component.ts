// TODO: karena child jadi tidak perlu OnInit
import { Component, EventEmitter, Output } from '@angular/core';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-add-todo-form',
  templateUrl: './add-todo-form.component.html',
  styleUrls: ['./add-todo-form.component.css']
})
export class AddTodoFormComponent {
  // TODO: deklarasiin bakal ada event emitter dengan data yang berbentuk model Todo output ngarah ke parent
  @Output() newTodoEvent = new EventEmitter<Todo>()

  // TODO: declare prop string
  inputTodo:string = ""

  addTodo() {
    const todo:Todo = {
      content: this.inputTodo,
      completed: false
    }

    // TODO: emit newTodoEvent ke parent dengan mengirimkan data object todo, nanti akan ditangkap oleh parent dalam bentuk variable bernama $event (cek sisi parent)
    this.newTodoEvent.emit(todo)
    this.inputTodo = ""
  }
}
