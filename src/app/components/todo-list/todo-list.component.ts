import { Component, OnInit } from '@angular/core';
import { Todo } from '../../models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  // properties:interface, ga perlu deklarasiin ini property apaan(?)
  todos:Todo[]=[];

  constructor() { }

  // TODO: inisiasi array object yang akan digunakan, disimpan di memory
  ngOnInit(): void {
    this.todos = [
      {
        content: 'todo pertama',
        completed: false
      },
      {
        content: 'todo kedua',
        completed: false
      },
    ]
  }

  toggleDone(id:number) {
    this.todos.map((v, i) => {
      if (i == id) v.completed = !v.completed
      console.log(v)
      return v
    })
  }

  addTodo(todo:Todo) {
    this.todos.push(todo)
  }

  deleteTodo(id:number) {
    this.todos = this.todos.filter((v, i) => i !== id)
  }

}
